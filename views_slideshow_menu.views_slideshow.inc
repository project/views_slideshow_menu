<?php

/**
 *  @file
 *  The default options available with Views Slideshow: Menu.
 */

function views_slideshow_menu_views_slideshow_modes() {
  $options = array(
    'menu' => t('Menu'),
  );
  return $options;
}

function views_slideshow_menu_views_slideshow_option_definition() {
  $options['menu'] =array(
    'contains' => array(
      'timeout' => array('default' => 5000),
      'sort' => array('default' => 1),
      'effect' => array('default' => 'fade'),
      'speed' => array('default' => 700), //normal
      'ie' => array(
        'contains' => array(
          'cleartype' => array('default' => TRUE),
          'cleartypenobg' => array('default' => FALSE),
        ),
      ),
      'advanced' => array('default' => ''),
    ),
  );
  return $options;
}

function views_slideshow_menu_views_slideshow_options_form(&$form, &$form_state, &$view) {
  $form['menu'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu rotator options'),
    '#collapsible' => TRUE,
    '#attributes' => array('class' => 'menu'),
  );

  $result = db_query("SELECT * FROM {menu_custom} ORDER BY title");
  $options = array();
  while($menu = db_fetch_array($result)) {
    $options[$menu['menu_name']] = $menu['title'];
  }
  $form['menu']['menus'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Menu Blocks'),
    '#options' => $options,
    '#default_value' => (isset($view->options['menu']['menus'])) ? $view->options['menu']['menus'] : array(),
    '#description' => t('Choose the menus you want the slideshow to associate with. These menus will have to be added to the page as a block.  If this does not work consider using the Menu Selectors field below to select a menu.'),
  );
  $form['menu']['menu_selectors'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu Selectors'),
    '#default_value' => (isset($view->options['menu']['menu_selectors'])) ? $view->options['menu']['menu_selectors'] : '',
    '#description' => t('Add additional css selectors to choose menus that are not in blocks. Ex. .primary-links, .secondary-links'),
  );
  $form['menu']['timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Timer delay'),
    '#default_value' => (isset($view->options['menu']['timeout'])) ? $view->options['menu']['timeout'] : 1000,
    '#description' => t('Amount of time in milliseconds between transitions.')
  );
  $form['menu']['delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Initial slide delay offset'),
    '#default_value' => (isset($view->options['menu']['delay'])) ? $view->options['menu']['delay'] : 0,
    '#description' => t('Amount of time in milliseconds for the first slide to transition. This number will be added to Timer delay to create the initial delay.  For example if Timer delay is 4000 and Initial delay is 2000 then the first slide will change at 6000ms (6 seconds).  If Initial delay is -2000 then the first slide will change at 2000ms (2 seconds).')
  );
  $form['menu']['speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Speed'),
    '#default_value' => (isset($view->options['menu']['speed'])) ? $view->options['menu']['speed'] : 700,
    '#description' => t('Time in milliseconds that each transition lasts. Numeric only!'),
  );
  $form['menu']['random'] = array(
    '#type' => 'radios',
    '#title' => t('Random'),
    '#description' => t('This option controls the order items are displayed. The default setting, False, uses the views ordering. True will cause the images to display in a random order.'),
    '#options' => array(0 => t('False'), 1 => t('True')),
    '#default_value' => isset($view->options['menu']['random'])? $view->options['menu']['random'] : 0,
  );
  $form['menu']['pause'] = array(
    '#type' => 'radios',
    '#title' => t('Pause'),
    '#options' => array(1 => t('Yes'),2 => t('No')),
    '#default_value' => (isset($view->options['menu']['pause'])) ? $view->options['menu']['pause'] : 1,
    '#description' => t('Pause when hovering on the slideshow image.'),
  );
  $options = array(
    'none' => 'none',
    'blindX' => 'blindX',
    'blindY' => 'blindY',
    'blindZ' => 'blindZ',
    'cover' => 'cover',
    'curtainX' => 'curtainX',
    'curtainY' => 'curtainY',
    'fade' => 'fade',
    'fadeZoom' => 'fadeZoom',
    'growX' => 'growX',
    'growY' => 'growY',
    'scrollUp' => 'scrollUp',
    'scrollDown' => 'scrollDown',
    'scrollLeft' => 'scrollLeft',
    'scrollRight' => 'scrollRight',
    'scrollHorz' => 'scrollHorz',
    'scrollVert' => 'scrollVert',
    'shuffle' => 'shuffle',
    'slideX' => 'slideX',
    'slideY' => 'slideY',
    'toss' => 'toss',
    'turnUp' => 'turnUp',
    'turnDown' => 'turnDown',
    'turnLeft' => 'turnLeft',
    'turnRight' => 'turnRight',
    'uncover' => 'uncover',
    'wipe' => 'wipe',
    'zoom' => 'zoom'
  );
  $form['menu']['effect'] = array(
    '#type' => 'select',
    '#title' => t('Effect'),
    '#options' => $options,
    '#default_value' => (isset($view->options['menu']['effect'])) ? $view->options['menu']['effect'] : 'fade',
    '#description' => t('The transition effect that will be used to change between images. Not all options below may be relevant depending on the effect.'),
  );
  $form['menu']['sync'] = array(
    '#type' => 'radios',
    '#title' => t('Sync'),
    '#options' => array(1 => t('Yes'), 2 => t('No')),
    '#default_value' =>(isset($view->options['menu']['sync'])) ? $view->options['menu']['sync'] : 1,
    '#description' => t('The sync option controls whether the slide transitions occur simultaneously. The default is yes which means that the current slide transitions out as the next slide transitions in. By setting the sync option to no you can get some interesting twists on your transitions.'),
  );
  $form['menu']['advanced'] = array(
    '#type' => 'textarea',
    '#title' => t('Advanced Options'),
    '#default_value' =>(isset($view->options['menu']['advanced'])) ? $view->options['menu']['advanced'] : '',
    '#description' => t('Add other jQuery cycle options one per line.  Ex. height: 350  !url', array('!url' => l('Click here for the additional options to add.', 'http://malsup.com/jquery/cycle/options.html'))),
  );
  $form['menu']['ie'] = array(
    '#type' => 'fieldset',
    '#title' => t('Internet Explorer Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['menu']['ie']['cleartype'] = array(
    '#type' => 'radios',
    '#title' => t('ClearType'),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#default_value' =>(isset($view->options['menu']['ie']['cleartype'])) ? $view->options['menu']['ie']['cleartype'] : 'true',
    '#description' => t('True if clearType corrections should be applied (for IE).  Some background issues could be fixed by setting this to false.'),
  );
  $form['menu']['ie']['cleartypenobg'] = array(
    '#type' => 'radios',
    '#title' => t('ClearType Background'),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#default_value' =>(isset($view->options['menu']['ie']['cleartypenobg'])) ? $view->options['menu']['ie']['cleartypenobg'] : 'false',
    '#description' => t('Set to true to disable extra cleartype fixing (leave false to force background color setting on slides)'),
  );
}

function views_slideshow_menu_views_slideshow_options_form_validate(&$form, &$form_state, &$view) {
  if (!is_numeric($form_state['values']['style_options']['menu']['speed'])) {
    form_error($form['menu']['speed'], t('!setting must be numeric!',array('Speed')));
  }
  if (!is_numeric($form_state['values']['style_options']['menu']['timeout'])) {
    form_error($form['menu']['speed'], t('!setting must be numeric!',array('timeout')));
  }
}
