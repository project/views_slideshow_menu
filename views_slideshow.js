
/**
 *  @file
 *  A simple jQuery menu Div Slideshow Rotator.
 */

/**
 *  This will set our initial behavior, by starting up each individual slideshow.
 */
Drupal.behaviors.viewsSlideshowMenu = function (context) {

  $('.views_slideshow_menu_main:not(.viewsMenu-processed)', context).addClass('viewsSlideshowMenu-processed').each(function() {
    var fullIdName = $(this).attr('id');
    var fullId = '#' + fullIdName;
    var settings = Drupal.settings.viewsSlideshowMenu[fullId];
    var targetIdName = $(fullId + " :first").attr('id');
    settings.targetId = '#' + targetIdName;

    settings.opts = {
      speed:settings.speed,
      timeout:parseInt(settings.timeout),
      delay:parseInt(settings.delay),
      sync:settings.sync==1,
      pause:settings.pause==1,
      random:settings.random==1,
      before:function(curr, next, opts) {
	$(settings.menu_selectors).each(function() {
	  $(this).find('a').parent().removeClass('activeSlideMenu');
	  $(this).find('.' + fullIdName + '-slide-' + opts.currSlide).addClass('activeSlideMenu');
	});
      },
      cleartype:(settings.ie.cleartype),
      cleartypeNoBg:(settings.ie.cleartypenobg)
    }

    if (settings.effect == 'none') {
      settings.opts.speed = 1;
    }
    else {
      settings.opts.fx = settings.effect;
    }

    /**
     * Add additional settings.
     */
    var advanced = settings.advanced.split("\n");
    for (i=0; i<advanced.length; i++) {
      var prop = '';
      var value = '';
      var property = advanced[i].split(":");
      for (j=0; j<property.length; j++) {
        if (j == 0) {
          prop = property[j];
        }
        else if (j == 1) {
          value = property[j];
        }
        else {
          value += ":" + property[j];
        }
      }
      settings.opts[prop] = value;
    }

    $(fullId + ' a').each(function(i) {
      var slideUrl = $(this).attr('href');
      $(settings.menu_selectors).each(function() {
	var menuLink = $(this).find('a[@href=' + slideUrl + ']');
	if ($(menuLink).text()) {
	  $(menuLink).parent().addClass(fullIdName + '-slide-' + i).hover(function() {
	    $(settings.targetId).cycle(i);
	  }, function() {

	  });
	}
      });
    });
    $(settings.targetId).cycle(settings.opts);

  });
}
